package com.ci.myShop.model;

public class Paper extends Consumable{
	String quality = " LOW, MEDIUM, HIGH";
	Float weight = (float) 0.0; //poids en gramme d’une feuille
	
	

	public Paper(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}



	public String getQuality() {
		return quality;
	}



	public void setQuality(String quality) {
		this.quality = quality;
	}



	public Float getWeight() {
		return weight;
	}



	public void setWeight(Float weight) {
		this.weight = weight;
	}

	
}
