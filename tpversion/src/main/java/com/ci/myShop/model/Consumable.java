package com.ci.myShop.model;

public class Consumable extends Item{
	int quantity;
	
	public Consumable(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
	
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


}