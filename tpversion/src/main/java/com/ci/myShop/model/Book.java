package com.ci.myShop.model;

public class Book extends Item{
	int nbPage;
	String author;
	String publisher;
	int year;
	int age; //age minimal pour lire ce livre
	int quantity;

	public Book(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}

	public int getNbPage() {
		return nbPage;
	}

	public void setNbPage(int nbPage) {
		this.nbPage = nbPage;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	

}
